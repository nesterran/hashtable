package hashtable

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestHashTable_Append(t *testing.T) {
	for _, tc := range appendTestCases {
		t.Run(tc.description, func(t *testing.T) {
			var actual HashTable
			for _, ele := range tc.input {
				actual.Append(ele.key, ele.value)
			}
			assert.EqualValues(t, tc.expected, actual, "the tables are not equal %s", tc.description)
		})
	}
}

func TestHashTable_Delete(t *testing.T) {

	for _, tc := range deleteTestCases {
		t.Run(tc.description, func(t *testing.T) {
			tb := &tc.original
			for _, i := range tc.input {
				tb.Delete(i)
			}

			assert.EqualValues(t, tc.expected, tc.original, "failed deleting on %s", tc.description)
		})
	}
}

func TestHashTable_Find(t *testing.T) {

	for _, tc := range findTestCases {
		t.Run(tc.description, func(t *testing.T) {
			tb := &tc.original
			v := tb.Find(tc.input)
			assert.Equal(t, tc.expected, v, "failed deleting on %s", tc.description)
		})
	}
}

func BenchmarkHashTable_Append(b *testing.B) {
	var h HashTable

	for _, tc := range appendTestCases {
		b.Run(tc.description, func(b *testing.B) {
			for _, ele := range tc.input {
				for i := 0; i < b.N; i++ {
					h.Append(ele.key, ele.value)
				}
			}
		})

	}
}

func BenchmarkHashTable_Find(b *testing.B) {
	for _, tc := range findTestCases {
		b.Run(tc.description, func(b *testing.B) {
			tb := &tc.original
			for i := 0; i < b.N; i++ {
				tb.Find(tc.input)
			}

		})

	}
}

func BenchmarkHashTable_Delete(b *testing.B) {
	for _, tc := range deleteTestCases {
		b.Run(tc.description, func(b *testing.B) {
			for _, v := range tc.input {
				for i := 0; i < b.N; i++ {
					tb := tc.original
					tb.Delete(v)
				}
			}
		})

	}
}

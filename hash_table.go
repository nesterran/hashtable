package hashtable

import (
	"hash/fnv"
	"math"
)

const defaultSize = 3
const minLoadFactor = 0.25
const maxLoadFactor = 0.75

type Element struct {
	hash uint16
	key  string
	val  string
	next *Element
}

type Table struct {
	elements []*Element
}

type HashTable struct {
	table     Table
	nElements int
}

func (t *HashTable) Append(k string, v string) {
	if t.table.elements == nil {
		*t = createTable(defaultSize)
	}
	t.append(hash(k), k, v)
	t.updateTableSize()

	return
}

func (t *HashTable) Find(k string) string {
	if ele := t.find(k); ele != nil {
		return (*ele).val
	}

	return ""
}

func (t *HashTable) Delete(k string) {
	if element := t.find(k); element != nil {
		*element = nil
		t.nElements--
		t.updateTableSize()
	}

	return
}

func hash(s string) uint16 {
	h := fnv.New64()
	_, _ = h.Write([]byte(s))
	return uint16(h.Sum64())
}

func (t *HashTable) updateTableSize() {
	if t.nElements == 0 {
		return
	}
	var newTable HashTable

	if newSize, changed := t.calculateSize(); !changed {
		return
	} else {
		newTable = createTable(newSize)
	}

	for _, element := range t.table.elements {
		if element == nil {
			continue
		}

		for element.next != nil {
			newTable.append(element.hash, element.key, element.val)
			element = element.next
		}

		newTable.append(element.hash, element.key, element.val)
	}
	*t = newTable
	return
}

func (t *HashTable) append(h uint16, k string, v string) {
	index := int(h) % len(t.table.elements)
	element := t.table.elements[index]
	newElement := Element{h, k, v, nil}

	if element == nil {
		t.table.elements[index] = &newElement
		t.nElements++
		return
	}

	if element.key == k {
		t.table.elements[index].val = v
		return
	}

	for element.next != nil {
		element = element.next
	}

	element.next = &newElement
	t.nElements++
	return
}

func (t *HashTable) find(k string) **Element {
	index := int(hash(k)) % len(t.table.elements)
	element := &t.table.elements[index]

	for *element != nil {
		if (*element).key == k {
			return element
		}
		element = &(*element).next
	}
	return nil
}

func createTable(size int) HashTable {
	return HashTable{table: Table{elements: make([]*Element, size)}, nElements: 0}
}

func (t *HashTable) calculateSize() (int, bool) {
	loadFactor := float64(t.nElements) / float64(len(t.table.elements))

	if loadFactor < minLoadFactor {
		size := int(math.Ceil(float64(t.nElements / 2)))

		if size <= defaultSize {
			return defaultSize, true
		}
		return size, true
	}

	if loadFactor > maxLoadFactor {
		return t.nElements * 2, true
	}

	return 0, false

}

package hashtable

type element struct {
	key   string
	value string
}

var appendTestCases = []struct {
	description string
	input       []element
	expected    HashTable
}{
	{
		description: "empty",
		input: []element{
			{"", ""},
		},
		expected: HashTable{
			table:     Table{[]*Element{{8997, "", "", nil}, nil, nil}},
			nElements: 1,
		},
	},
	{
		description: "1 element",
		input: []element{
			{"BLU", "Blue"},
		},
		expected: HashTable{
			table:     Table{[]*Element{nil, {14692, "BLU", "Blue", nil}, nil}},
			nElements: 1,
		},
	},
	{
		description: "3 elements with duplicated key",
		input: []element{
			{"BLU", "Navy Blue"},
			{"BLU", "Blue"},
			{"RED", "Red"},
		},
		expected: HashTable{
			table:     Table{[]*Element{{53394, "RED", "Red", nil}, {14692, "BLU", "Blue", nil}, nil}},
			nElements: 2,
		},
	},
	{
		description: "3 elements with 1 coalition",
		input: []element{
			{"BLU", "Navy Blue"},
			{"RED", "Red"},
			{"PUR", "Purple"},
		},
		expected: HashTable{
			table:     Table{[]*Element{{53394, "RED", "Red", &Element{41514, "PUR", "Purple", nil}}, nil, nil, nil, {14692, "BLU", "Navy Blue", nil}, nil}},
			nElements: 3,
		},
	},
	{
		description: "4 elements with 1 coalition and 1x slice growing",
		input: []element{
			{"BLU", "Navy Blue"},
			{"RED", "Red"},
			{"PUR", "Purple"},
			{"GRE", "Green"},
		},
		expected: HashTable{
			table:     Table{[]*Element{{53394, "RED", "Red", &Element{41514, "PUR", "Purple", nil}}, nil, nil, nil, {14692, "BLU", "Navy Blue", nil}, {57707, "GRE", "Green", nil}}},
			nElements: 4,
		},
	},
	{
		description: "9 elements with 2 coalition and 3x slice growing",
		input: []element{
			{"BLU", "Navy Blue"},
			{"RED", "Red"},
			{"PUR", "Purple"},
			{"GRE", "Green"},
			{"YEL", "Yellow"},
			{"WHI", "White"},
			{"BLA", "Black"},
			{"AMB", "Amber"},
			{"PIN", "Pink"},
		},
		expected: HashTable{
			table:     Table{[]*Element{{14704, "BLA", "Black", nil}, nil, {53394, "RED", "Red", &Element{46738, "PIN", "Pink", nil}}, nil, {14692, "BLU", "Navy Blue", nil}, nil, nil, {4151, "AMB", "Amber", nil}, nil, {23481, "WHI", "White", &Element{44025, "YEL", "Yellow", nil}}, {41514, "PUR", "Purple", nil}, {57707, "GRE", "Green", nil}, nil, nil, nil, nil}},
			nElements: 9,
		},
	},
}

var deleteTestCases = []struct {
	description string
	input       []string
	original    HashTable
	expected    HashTable
}{
	{
		description: "1 element",
		input:       []string{"BLU"},
		original: HashTable{
			table:     Table{[]*Element{nil, {14692, "BLU", "Blue", nil}, nil}},
			nElements: 1,
		},
		expected: HashTable{
			table:     Table{[]*Element{nil, nil, nil}},
			nElements: 0,
		},
	},
	{
		description: "2 elements",
		input:       []string{"RED"},
		original: HashTable{
			table:     Table{[]*Element{{53394, "RED", "Red", nil}, {14692, "BLU", "Blue", nil}, nil}},
			nElements: 2,
		},
		expected: HashTable{
			table:     Table{[]*Element{nil, {14692, "BLU", "Blue", nil}, nil}},
			nElements: 1,
		},
	},
	{
		description: "Remove from linked list",
		input:       []string{"PUR"},
		original: HashTable{
			table:     Table{[]*Element{{53394, "RED", "Red", &Element{41514, "PUR", "Purple", nil}}, nil, nil, nil, {14692, "BLU", "Navy Blue", nil}, {57707, "GRE", "Green", nil}}},
			nElements: 4,
		},
		expected: HashTable{
			table:     Table{[]*Element{{53394, "RED", "Red", nil}, nil, nil, nil, {14692, "BLU", "Navy Blue", nil}, {57707, "GRE", "Green", nil}}},
			nElements: 3,
		},
	},
	{
		description: "shrink table",
		input:       []string{"GRE", "BLU"},
		original: HashTable{
			table:     Table{[]*Element{{53394, "RED", "Red", nil}, nil, nil, nil, {14692, "BLU", "Navy Blue", nil}, {57707, "GRE", "Green", nil}}},
			nElements: 3,
		},
		expected: HashTable{
			table:     Table{[]*Element{{53394, "RED", "Red", nil}, nil, nil}},
			nElements: 1,
		},
	},
}

var findTestCases = []struct {
	description string
	input       string
	original    HashTable
	expected    string
}{
	{
		description: "2 elements table",
		input:       "RED",
		original: HashTable{
			table:     Table{[]*Element{{53394, "RED", "Red", nil}, {14692, "BLU", "Blue", nil}, nil}},
			nElements: 2,
		},
		expected: "Red",
	},
	{
		description: "search in linked list",
		input:       "PUR",
		original: HashTable{
			table:     Table{[]*Element{{53394, "RED", "Red", &Element{41514, "PUR", "Purple", nil}}, nil, nil, nil, {14692, "BLU", "Navy Blue", nil}, {57707, "GRE", "Green", nil}}},
			nElements: 4,
		},
		expected: "Purple",
	},
	{
		description: "dont exist",
		input:       "YEL",
		original: HashTable{
			table:     Table{[]*Element{{53394, "RED", "Red", &Element{41514, "PUR", "Purple", nil}}, nil, nil, nil, {14692, "BLU", "Navy Blue", nil}, {57707, "GRE", "Green", nil}}},
			nElements: 4,
		},
		expected: "",
	},
}
